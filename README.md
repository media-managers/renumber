## Renumber

I created these to help me organize my music collection. 

### addash

adds a dash to songs that do not have the dash between number and name.

```
# Converts this:
01 Wish You Were Here.flac

# To this:
01 - Wish You Were Here.flac
```
----

### dotodash

changes a decimal to dash.

```
# Converts this:
01. Deal.flac

# To this:
01 - Deal.flac
```
----

### renumber

This is useful in multi-disc albums to continue the sequence
in filenames. Then you can move all the files to one directory for
gapless playback between discs.


#### What it do
1. Ask what number to start with.
2. Ask what the extension is. eg. flac, mp3
3. Loop through the directory and number the files.

I recommend keeping the discs separated until you're done numbering them, 
then `mv` them to one directory then optionally run the [media-tag](https://gitlab.com/chuckn246/media-tag) script on them to update the metadata.

A brief example:

```
Converts This:

# Disc One
01 - Song.flac
...
10 - Song.flac

# Disc Two
01 - SongDisc2.flac
...
10 - SongDisc2.flac

To This:

# Disc One
01 - Song.flac
...
10 - Song.flac

# Disc Two
11 - SongDisc2.flac
...
20 - SongDisc2.flac
```
